from pytest import fixture


from dot_convert.line import Line


@fixture(name='line1')
def line1_fixture() -> Line:
    return Line('A', 'B', dict(type='fancy', size='9'))


@fixture(name='line2')
def line2_fixture() -> Line:
    return Line('B', 'C', dict())


def test_str_with_attrs(line1: Line):
    assert str(line1) == "A -> B [type='fancy' size='9']"


def test_str_no_attrs(line2: Line):
    assert str(line2) == "B -> C []"


def test_eq(line1: Line):
    line1_copy = Line('A', 'B', dict(type='fancy', size='9'))

    assert line1 == line1_copy


def test_neq(line1: Line, line2: Line):
    assert line1 != line2

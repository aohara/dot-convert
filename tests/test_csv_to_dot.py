from pytest import fixture
from typing import IO
from io import StringIO

from dot_convert.csv_converter import CsvConverter
from dot_convert.line import Line


@fixture(name='csv1')
def csv1_fixture() -> IO:
    csv = """start,end,type
    A,B,fancy
    B,C,ugly
    """
    return StringIO(csv)


@fixture(name='csv2')
def csv2_fixture() -> IO:
    csv = (
        "start,end,type,size\n"
        "A,B,normal\n"
        "B,C,,big\n"
        "C,A,fancy,small"
    )
    return StringIO(csv)


@fixture(name='converter')
def converter_fixture() -> CsvConverter:
    converter = CsvConverter()
    converter.lines += [
        Line('A', 'B', dict(type='fancy')),
        Line('B', 'C', dict(type='ugly'))
    ]
    return converter


def test_parse(csv1: IO):
    expected = [
        Line('A', 'B', dict(type='fancy')),
        Line('B', 'C', dict(type='ugly'))
    ]

    converter = CsvConverter.parse(csv1)
    assert expected == converter.lines


def test_parse_nonuniform_attrs(csv2: IO):
    expected = [
        Line('A', 'B', dict(type='normal')),
        Line('B', 'C', dict(size='big')),
        Line('C', 'A', dict(type='fancy', size='small'))
    ]

    converter = CsvConverter.parse(csv2)
    assert expected == converter.lines


def test_converter_str(converter: CsvConverter):
    expected = (
        "A -> B [type='fancy']\n"
        "B -> C [type='ugly']"
    )
    assert expected == str(converter)

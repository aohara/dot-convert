from sys import argv
from os import path

from dot_convert.csv_converter import CsvConverter


def main(source_file: str):
    print(f'Loading {source_file}...', end='')
    with open(source_file, 'r') as f:
        print('done')

        print('Parsing...', end='')
        converter = CsvConverter.parse(f)
        print('done')

    output_filename = path.basename(source_file).split('.')[0] + '.dot'
    print(f'Saving {output_filename}...', end='')
    with open(output_filename, 'w') as f:
        for line in converter:
            f.write(str(line))
            f.write('\n')
        print('done')


if __name__ == '__main__':
    if len(argv) > 1:
        source_filename = argv[1]
        print(f'Will use {source_filename}')
    else:
        source_filename = 'data.csv'
        print(f'Source not provided.  Will default to {source_filename}')

    try:
        main(source_filename)
    except Exception as e:
        print(e)

    input('Press Enter to exit')

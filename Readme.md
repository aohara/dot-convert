# dot-convert


Imports files into Graphviz dot format

## Supported Formats

### CSV

Expected Format:

| start | end | attr1 (optional) | attr2 (optional) |
| ------ | ------ | ----- | ----- |
| A | B | foo | bar |
| B | C| | |

The attrs are optional, and you can include as many as you want.

Output:
```dot
A -> B [attr1='foo' attr2='bar']
B -> C []
```

## Download

Download a release [From Here](https://gitlab.com/aohara/dot-convert/tags)

## Usage

If no argument is provided, or if the exe is double-clicked, the program will default to data.csv

```commandline
C:\ dot-convert.exe
C:\ dot-convert.exe my-data.csv
```


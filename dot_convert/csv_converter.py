from typing import IO, Iterator
from csv import DictReader

from dot_convert.line import Line


class CsvConverter(object):

    def __init__(self):
        self.lines = []

    @classmethod
    def parse(cls, data: IO) -> 'CsvConverter':
        result = cls()
        result.read(data)
        return result

    def read(self, data: IO):
        for row in DictReader(data):
            start = row.pop('start')
            end = row.pop('end')

            if not start or not end:
                print(f'Warning: Invalid row was skipped: {row}')
                continue

            attrs = {
                key.strip(): value.strip()
                for key, value in row.items()
                if value
            }

            self.lines.append(Line(start.strip(), end.strip(), attrs))

    def __iter__(self) -> Iterator[Line]:
        return iter(self.lines)

    def __str__(self) -> str:
        lines = [str(line) for line in self]
        return '\n'.join(lines)

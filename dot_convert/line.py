from typing import Dict


class Line(object):

    def __init__(self, start: str, end: str, attrs: Dict[str, str]):
        self.start = start
        self.end = end
        self.attrs = attrs

    def __eq__(self, other):
        return repr(self) == repr(other)

    def __str__(self):
        attr_strings = [f"{key}='{value}'" for key, value in self.attrs.items()]
        attr_string = '[' + ' '.join(attr_strings) + ']'
        return f'{self.start} -> {self.end} {attr_string}'

    def __repr__(self):
        return str(self)
